# Proj0-Hello
-------------
Author: Andrew Evelhoch
Contact: aevelhoc@uoregon.edu
Description:
As assigned, "Trivial project to exercise version control, turn-in, and other mechanisms." In practice, it demonstrates that the student can use get git and python, despite
possibly having no prior experience. 'hello.py' itself simply prints a string from the credentials.ini, and in this example will print "Hello world"